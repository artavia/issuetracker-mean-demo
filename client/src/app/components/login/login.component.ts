import { 
  Component, OnInit 
  , Input
  , ChangeDetectorRef, AfterViewInit
} from '@angular/core';
import { 
  FormGroup, FormBuilder /* , Validators */
  , FormControl, Validators 
} from '@angular/forms';


import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

// import { MatFormFieldControl } from '@angular/material';
import { patternValidator } from 'src/app/shared/pattern-validator';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

// https://stackoverflow.com/questions/34364880/expression-has-changed-after-it-was-checked#35243106
export class LoginComponent implements OnInit, AfterViewInit {

  @Input('isError') isError;

  loginForm: FormGroup;
  customErrorSkerdoodle: any = false; // customErrorSkerdoodle: String = "";

  constructor( private auth: AuthService , private router: Router, private fb: FormBuilder, private cdr: ChangeDetectorRef ){
    
    
    /* const groupParam = {
      email: ''
      , password: ''
    };
    this.loginForm = this.fb.group( groupParam ); */

    const groupParam = {
      email: new FormControl( '', [ Validators.required, patternValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      password: new FormControl( '', Validators.required )
    };
    this.loginForm = new FormGroup( groupParam);

  }

  setCustomError(pm){
    if(pm === undefined){
      // this.customErrorSkerdoodle = "Testing 1,2,3. Testing. Testing.";
      this.customErrorSkerdoodle = "";
    }
    else
    if( pm !== undefined ){
      this.customErrorSkerdoodle = pm;
    }
  }

  ngOnInit() {
    this.isError = null;
    // this.customErrorSkerdoodle = "Testing 1,2,3. Testing. Testing.";
    this.setCustomError(undefined);
  }

  loginUser( obj ){ 

    this.customErrorSkerdoodle = "";
    
    // console.log( "obj" , obj ); 
    // let obj = { email, password }; // console.log( "obj" , obj );

    this.auth.loginUser( obj ).subscribe(
      res => { 
        // console.log( "res", res);

        if(!!res.errormessage){
          // console.log( "res.errormessage", res.errormessage );
          this.setCustomError( res.errormessage );
        }
        if(!res.errormessage){ 
          // console.log( "loginUser res ", res );

          localStorage.setItem( 'token' , res.token );

          this.router.navigate( [ '/special' ] );
        }
      }
      , err => {
        
        // console.log( "loginUser err: ", err );
        // console.log( "loginUser err.error.errormessage: ", err.error.errormessage );

        this.setCustomError(err.error.errormessage);
        
        // if( err.status !== 401 || err.status !== 404 ){
        this.isError = err;
        // }

      } 
    ); 
    
  }

  ngAfterViewInit(){
    this.cdr.detectChanges();
  }

}