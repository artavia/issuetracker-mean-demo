import { 
  Component, OnInit 
  , Input
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


import { IssueService } from '../../services/issue.service';
import { Router } from '@angular/router';

import { HelperfunctionsService } from 'src/app/services/helperfunctions.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  @Input('isError') isError;

  createForm: FormGroup;
  massageThePhrase: any = false; // massageThePhrase: string = "";

  constructor( private issueService: IssueService, private fb: FormBuilder, private router: Router, private hfs: HelperfunctionsService ) {
    
    const groupParam = {
      title: [ '' , Validators.required ]
      , responsible: ''
      , description: ''
      , severity: ''
    };
    this.createForm = this.fb.group( groupParam );

    this.massageThePhrase = this.hfs.massageThePhrase;

  }

  ngOnInit() {
    this.isError = null;
  }

  // addIssue( title, responsible, description, severity ){
  addIssue( obj ){

    let { title, responsible, description, severity } = obj;

    let newobj = {
      title: this.massageThePhrase( title )
      , responsible
      , description: this.massageThePhrase( description )
      , severity
    };
    
    // this.issueService.addIssue( title, responsible, description, severity ).subscribe( () => { this.router.navigate( ['/list'] ) } );
    this.issueService.addIssue( newobj ).subscribe( 
      ( res ) => { 

        // console.log( "create component addIssue() method ~ res " , res ); // { message: "Issue has been added" }
        
        this.router.navigate( ['/list'] );

      } 
      , (err) => {
        console.log( "addIssue err", err );
        this.isError = err;
      }
    );
  }

}
