import { 
  Component, OnInit 
  , Input
  , ChangeDetectorRef, AfterViewInit
} from '@angular/core';
import { 
  FormGroup, FormBuilder /* , Validators */ 
  , FormControl, Validators
} from '@angular/forms';


import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

// import { MatFormFieldControl } from '@angular/material';
import { patternValidator } from 'src/app/shared/pattern-validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
// https://stackoverflow.com/questions/34364880/expression-has-changed-after-it-was-checked#35243106
export class RegisterComponent implements OnInit, AfterViewInit {

  @Input('isError') isError;

  registerForm: FormGroup;
  customErrorSkerdoodle: any = false; // customErrorSkerdoodle: String = "";

  constructor( private auth: AuthService , private router: Router, private fb: FormBuilder, private cdr: ChangeDetectorRef ){

    /* const groupParam = {
      email: ''
      , password: ''
    };
    this.registerForm = this.fb.group( groupParam ); */


    const groupParam = { 
      email: new FormControl( '', [ Validators.required, patternValidator(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      password: new FormControl( '', Validators.required )
    };
    this.registerForm = new FormGroup( groupParam );

  }

  setCustomError(pm){
    if(pm === undefined){
      // this.customErrorSkerdoodle = "Testing 1,2,3. Testing. Testing.";
      this.customErrorSkerdoodle = "";
    }
    else
    if( pm !== undefined ){
      this.customErrorSkerdoodle = pm;
    }
  }

  ngOnInit() {
    this.isError = null;
    // this.customErrorSkerdoodle = "Testing 1,2,3. Testing. Testing.";
    this.setCustomError(undefined);
  }

  registerUser( obj ){ 

    this.customErrorSkerdoodle = "";

    // console.log( "obj" , obj );   
    // let obj = { email, password }; // console.log( "obj" , obj );
    
    this.auth.getAllUsers().subscribe(
      
      res => {
        
        // console.log( "registerUser res ", res );

        // set state error: false, ETC., ETC.        
        let value;
        const doesEmailExist = res.some( user => user.email === obj.email );
        if( doesEmailExist ){ value = true; }
        if( !doesEmailExist ){ value = false; }
        if( value === true ){
          // set state error: false, ETC., ETC.
          // console.log( "Email address exists. Try again!" );
          this.setCustomError( "Email address exists. Try again!" );
        }
        if( value === false ){
          
          // console.log( "obj >>> and, this is where the next ITEM goes!" , obj );
          
          this.auth.registerUser( obj ).subscribe( 
            res => {
              if(!!res.errormessage){
                // console.log( "res.errormessage", res.errormessage );
                this.setCustomError( res.errormessage );
              }
              if(!res.errormessage){
                // console.log( "registerUser res ", res ); // { message: "User has been added" }
                

                this.router.navigate( [ '/login' ] );
              }
            }
            , err => {
              // console.log( "registerUser err:", err );
              // console.log( "registerUser err.error.errormessage: ", err.error.errormessage );
              this.setCustomError(err.error.errormessage);
              
              this.isError = err;

            }
          );
        }
        // return res;
      }
      , err => { 
        // console.log( "getAllUsers err: ", err );
        
        this.isError = err;
        
      }
    ); 
    
  }
  

  ngAfterViewInit(){
    this.cdr.detectChanges();
  }
  // end  
}