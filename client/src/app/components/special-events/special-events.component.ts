import { 
  Component, OnInit 
  , Input
} from '@angular/core';

import { Specialevent } from 'src/app/models/specialevent';
import { EventService } from 'src/app/services/event.service';

import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-special-events',
  templateUrl: './special-events.component.html',
  styleUrls: ['./special-events.component.scss']
})
export class SpecialEventsComponent implements OnInit {

  @Input('isError') isError;

  specialEvents: Specialevent [];

  constructor( private eventService: EventService, private router: Router ) { }

  ngOnInit() {
    this.isError = null;
    this.getSpecEvts();
  }

  getSpecEvts(){ 

    this.eventService.getSpecialEvents().subscribe(
      ( data: Specialevent[] ) => { 
        this.specialEvents = data;
      }
      , err => {
        console.log( "specialEvents err: ", err );
        
        this.isError = err;
        
        if( err instanceof HttpErrorResponse ){
          if( err.status === 401 ){
            this.router.navigate( [ '/login' ] );
          }
        }
      } 
    );

  }
}