import { 
  Component, OnInit 
  , Input
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatSnackBar } from '@angular/material';
import { IssueService } from '../../services/issue.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @Input('isError') isError;

  id: String;
  issue: any = {};
  updateForm: FormGroup;

  constructor( private issueService: IssueService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar, private fb: FormBuilder ) {
    
    this.runUpdateForm();

  }

  runUpdateForm(){
    const groupParam = {
      title: [ '' , Validators.required ], responsible: '', description: '', severity: '', status: '' 
    };
    this.updateForm = this.fb.group( groupParam );
  }

  ngOnInit() {
    
    this.isError = null;

    this.route.params.subscribe( 

      (params) => {
        this.getIssueById( params );
      } 
      , (err) => {
        console.log( "edit component ~ ngOnInit err", err );
        this.isError = err;
      }

    );
  }

  getIssueById(params){
    
    this.id = params.id;

    this.issueService.getIssueById( this.id ).subscribe( 
      
      (res) => {

        this.issue = res;
        this.updateForm.get('title').setValue( this.issue.title );
        this.updateForm.get('responsible').setValue( this.issue.responsible );
        this.updateForm.get('description').setValue( this.issue.description );
        this.updateForm.get('severity').setValue( this.issue.severity );
        this.updateForm.get('status').setValue( this.issue.status );

      } 
      , (err) => {
        console.log( "getIssueById err", err );
        this.isError = err;
      }
    );
  }

  // updateIssue( title, responsible, description, severity, status ){
  updateIssue( obj ){

    // this.issueService.updateIssue( this.id, title, responsible, description, severity, status ).subscribe( () => {
    //   this.snackBar.open( 'Issue updated successfully' , 'OK', { duration: 2812 } );
    // } );
    // let { title, responsible, description, severity, status } = obj;

    let updateObj = { id: this.id , ...obj };

    // this.issueService.updateIssue( this.id, title, responsible, description, severity, status )
    this.issueService.updateIssue( updateObj )
    .subscribe( ( res ) => {
      
      // console.log( "edit component updateIssue() method ~ res " , res ); // { message: "Issue has been updated" }

      let message  = res['message'];  // console.log( 'message: ' , message ); 
      // this.snackBar.open( 'Issue updated successfully' , 'OK', { duration: 2812 } );
      this.snackBar.open( message , 'OK', { duration: 2812 } );

    } );

  }

}

