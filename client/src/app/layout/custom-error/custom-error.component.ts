import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custom-error',
  templateUrl: './custom-error.component.html',
  styleUrls: ['./custom-error.component.scss']  
})
export class CustomErrorComponent implements OnInit {

  @Input('isError') isError;

  constructor() { }

  ngOnInit() {
  }

}

