import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-toolbar-multirow',
  templateUrl: './toolbar-multirow.component.html',
  styleUrls: ['./toolbar-multirow.component.scss']
})
export class ToolbarMultirowComponent implements OnInit {
  
  // constructor( private authService: AuthService ) { }
  constructor( public authService: AuthService ) { } // for [ enableProd()\ng build --prod ] purposes

  ngOnInit() {
  }

}
