export interface Specialevent { 
  id: String;

  name: String;
  description: String;
  date: Date;
}
