export interface Event { 
  id: String;

  name: String;
  description: String;
  date: Date;
}
