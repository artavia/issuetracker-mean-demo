import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperfunctionsService {

  constructor() { }

  // custom string helper function
  massageThePhrase( str ){
    let offthefrontandback = str.trim();
    // let newstring = offthefrontandback.replace(/\s\s+/g, ' '); // alt 1
    let newstring = offthefrontandback.replace(/  +/g, ' '); // alt 2
    const firstChar = newstring.slice(0,1).toUpperCase();
    // const otherChars = newstring.slice(1).toLowerCase();
    const otherChars = newstring.slice(1);
    newstring = firstChar + otherChars;
    return newstring;
  }
  
}
