import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
// import { BaseurlService } from './baseurl.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // baseUrl:string = "";
  
  private registerUrl: string = ""; 
  private loginUrl: string = ""; 
  private usersUrl: string = ""; 

  constructor( private http: HttpClient , private router: Router /* , private baseurlservice : BaseurlService */ ){ 

    // this.baseUrl = this.baseurlservice.baseUrl;
    
    // "http://localhost:3000/api/users"; 
    this.usersUrl = "/api/users";                          // YES WITH PROXY
    // this.usersUrl = `${this.baseUrl}/api/users`;              // YES
    
    // "http://localhost:3000/api/register";
    this.registerUrl = "/api/register";                    // YES WITH PROXY
    // this.registerUrl = `${this.baseUrl}/api/register`;        // YES
    
    // "http://localhost:3000/api/login"; 
    this.loginUrl = "/api/login";                          // YES WITH PROXY
    // this.loginUrl = `${this.baseUrl}/api/login`;              // YES
    
  }

  // This is new in order to facilitate original users
  getAllUsers(){
    return this.http.get<any>( this.usersUrl );
  }
  
  registerUser( user ){
    return this.http.post<any>( this.registerUrl , user );
  }

  loginUser( user ){
    return this.http.post<any>( this.loginUrl , user );
  }

  logoutUser(){
    localStorage.removeItem('token'); 
    // this.router.navigate( [ '/events' ] );
    this.router.navigate( [ '/list' ] );
  }

  getToken(){
    return localStorage.getItem('token');
  }

  loggedIn(){
    return !!localStorage.getItem('token');
  }
}
