import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { BaseurlService } from './baseurl.service';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  // baseUrl:string = "";

  private issuesUrl: string = ""; 

  constructor( private http: HttpClient /* , private baseurlservice : BaseurlService */ ) { 

    // this.baseUrl = this.baseurlservice.baseUrl;

    // "http://localhost:3000/issues"; 

    this.issuesUrl = "/issues";                          // YES WITH PROXY
    // this.issuesUrl = `${this.baseUrl}/issues`;              // YES

  }

  getIssues(){
    return this.http.get( `${this.issuesUrl}` );
  }

  getIssueById(id){
    return this.http.get( `${this.issuesUrl}/${id}` );
  }

  addIssue( obj ){

    let { title, responsible, description, severity } = obj;
    
    const issue = {
      title: title, responsible: responsible, description: description, severity: severity 
    };

    return this.http.post( `${this.issuesUrl}/add` , issue );
  }

  updateIssue( obj ){
    
    let { id, title, responsible, description, severity, status } = obj;

    const issue = {
      title: title, responsible: responsible, description: description, severity: severity, status: status 
    };

    return this.http.post( `${this.issuesUrl}/update/${id}` , issue );
  }

  deleteIssue( id ){
    return this.http.get( `${this.issuesUrl}/delete/${id}` );
  }

}
