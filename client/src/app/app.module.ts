import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { 
  HttpClientModule
  , HTTP_INTERCEPTORS 
} from '@angular/common/http';

import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { 
  MatToolbarModule, MatIconModule 
} from '@angular/material';
import { 
  MatFormFieldModule, MatInputModule
  , MatOptionModule, MatSelectModule, MatButtonModule, MatCardModule, MatTableModule, MatDividerModule, MatSnackBarModule 
} from '@angular/material';

// import {} from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

// import { EventsComponent } from './components/events/events.component';
import { SpecialEventsComponent } from './components/special-events/special-events.component';

import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';

import { ToolbarMultirowComponent } from './layout/toolbar-multirow/toolbar-multirow.component';
import { CustomErrorComponent } from './layout/custom-error/custom-error.component';

import { TokenInterceptorService } from './services/token-interceptor.service';
import { CustomFooterComponent } from './layout/custom-footer/custom-footer.component';

import { DNEPageComponent } from './components/d-n-e-page/d-n-e-page.component';

@NgModule({
  declarations: [
    AppComponent 
    
    , LoginComponent, RegisterComponent
    // , EventsComponent
    , SpecialEventsComponent

    , ListComponent, CreateComponent, EditComponent

    , ToolbarMultirowComponent
    , CustomErrorComponent
    , CustomFooterComponent
    , DNEPageComponent 
  ],
  imports: [
    BrowserModule 
    , HttpClientModule 
    , ReactiveFormsModule 
    , FormsModule 
    , AppRoutingModule 
    , BrowserAnimationsModule 

    , MatToolbarModule , MatIconModule 
    , MatFormFieldModule, MatInputModule 
    , MatOptionModule, MatSelectModule, MatButtonModule, MatCardModule, MatTableModule, MatDividerModule
    , MatSnackBarModule 
    
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS
      , useClass: TokenInterceptorService
      , multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
