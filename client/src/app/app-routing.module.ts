import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from "./components/list/list.component";
import { CreateComponent } from "./components/create/create.component";
import { EditComponent } from "./components/edit/edit.component";

// import { EventsComponent } from './components/events/events.component';

import { SpecialEventsComponent } from './components/special-events/special-events.component';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';

import { AuthGuard } from './guards/auth.guard';

import { DNEPageComponent } from './components/d-n-e-page/d-n-e-page.component';

const routes: Routes = [

  // { path: '', redirectTo: '/events' , pathMatch: 'full' }
  // , { path: 'events' , component: EventsComponent } , 

  {
    path: 'special' 
    , canActivate: [ AuthGuard ] 
    , component: SpecialEventsComponent
  }
  
  , 
  { path: '', redirectTo: '/list' , pathMatch: 'full' }
  , { path: 'list' , component: ListComponent }
  // { path: '' , component: ListComponent } // Note to self... don't do this
  
  , { path: 'create' , canActivate: [ AuthGuard ] , component: CreateComponent }
  , { path: 'edit/:id' , canActivate: [ AuthGuard ] , component: EditComponent } 

  , { path: 'login' , component: LoginComponent }
  , { path: 'register' , component: RegisterComponent }

  , {path: '404' , component: DNEPageComponent }
  , { path: '**' , redirectTo: '/404' }
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
