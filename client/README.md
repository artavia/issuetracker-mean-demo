# Description
Prototype issue tracker-- MEAN stack exercise --for use with Heroku and Mongo Atlas.

## Please visit The Issue App
You can ~~abuse~~ experiment with and/or view [the demo which is found at heroku](https://issuetracker-mean-demo.herokuapp.com "Link to MERN Todo App") and decide if this is something for you to play around with at a later date.

## An observation unique to deploying Angular Apps to Heroku
It can be a chore, hence, I would like to offer a few tips in order to avoid pain points come **&quot;go time!!!&quot;** Here we go. The following changes and modifications to common dev workflow are necessary in order to facilitate the wanted outcome when uploading through the cli.

  - Under **package.scripts** you need to add a **postinstall** script in order to deploy properly.

```sh
  {
    "scripts": {
      "postinstall": "ng build --aot --prod"
    },
  }
```

  - There are a number of pre-installed dependencies in your Angular project that you will be responsible for transplanting and whatever that may involve. Change their locations accordingly.

    - This what it looked like before the change. So, not quite.
    
    ```sh
      {
        "devDependencies": {
          "@angular-devkit/build-angular": "~0.803.19",
          "@angular/cli": "~8.3.19",
          "@angular/compiler-cli": "~8.2.14",
          "typescript": "~3.5.3"
        },
      }
    ```
    - And, this is what it looked like after the change. Now you are ready.

    ```sh
      {
        "dependencies": {
          "@angular-devkit/build-angular": "~0.803.19",
          "@angular/cli": "~8.3.19",
          "@angular/compiler-cli": "~8.2.14",
          "typescript": "~3.5.3"
        },
      }
    ```
# Go have some fun.
I wish everybody a Happy New Year in 2020.