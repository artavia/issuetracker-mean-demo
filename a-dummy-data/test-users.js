db.issues.drop();
db.users.drop();
db.events.drop();
db.specialevents.drop();

db.events.insert( {
	name : "Alpha Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Beta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Charlie Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Delta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Elephant Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.events.insert( {
	name : "Foxtrot Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Alpha Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Beta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Charlie Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Delta Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Elephant Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );

db.specialevents.insert( {
	name : "Special Foxtrot Expo"
	, description: "lorem ipsum"
	, date : new Date( 2012 , 3, 4, 5, 6, 7 )
} );



db.issues.insert( {
	status : "Open"
  , title : "A sudden need for staples"
	, responsible : "Lucho"
	, description : "Staple some more papers together because chances are I was going to do so anyways."
  , severity : "High"
} ); 

db.issues.insert( {
	status : "Open"
  , title : "Freezerbox issues"
	, responsible : "Lucho"
	, description : "Ran out of chocolate ice cream."
  , severity : "High"
} ); 

db.issues.insert( {
	status : "Open"
  , title : "Contemplating the number of sheets of TP in the house at midnight"
	, responsible : "Lucho"
	, description : "There are 6 sheets of toilet paper that have to carry us over until 7am tomorrow morning."
  , severity : "Medium"
} ); 

db.issues.insert( {
	status : "Done"
  , title : "Hot wiring"
	, responsible : "Lucho"
	, description : "Discover the 101st way to hot wire an automobile."
  , severity : "Low"
} ); 

db.issues.insert( {
	status : "Open"
  , title : "Musical curiosities on display"
	, responsible : "Lucho"
	, description : "Learning to play the banjo, the tuba, and the timpani simultaneously notwithstanding present issues with obtaining the resources to achieve such a feat at all!"
  , severity : "High"
} ); 

db.issues.insert( {
	status : "In Progress"
  , title : "Knock knock"
	, responsible : "Lucho"
	, description : "Salami who? You cannot experience salami without mortadella."
  , severity : "Medium"
} ); 

db.issues.insert( {
	status : "Open"
  , title : "Bake a cake"
	, responsible : "Lucho"
	, description : "Apple Caramel sounds just about right."
  , severity : "Medium"
} ); 

db.issues.insert( {
	status : "Open"
  , title : "Buy earplugs"
	, responsible : "Lucho"
	, description : "When Tío goes to the bathroom he sounds at times like a horse in a stall."
  , severity : "High"
} ); 
// fin. of all inserts for any collection