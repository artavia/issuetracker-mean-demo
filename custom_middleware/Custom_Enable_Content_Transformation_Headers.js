module.exports = (req, res, next) => { 

  // ----------------------------------------------------------------------
  // | Content transformation                                             |
  // | PERFORMANCE helper function
  // | Prevent intermediate caches or proxies (e.g.: such as the ones used
  // | by mobile network providers) from modifying the website's content.
  // | https://developers.google.com/speed/pagespeed/module/configuration#notransform
  // | https://stackoverflow.com/questions/4480304/how-to-set-http-headers-for-cache-control
  // | https://cloud.google.com/storage/docs/transcoding
  // ----------------------------------------------------------------------

  res.set( 'Cache-Control' , 'private, no-cache, no-store, proxy-revalidate, no-transform' );
  
  var secs = 0;
  // var expr = 'public, max-age='+ '' + secs +'';
  // res.set( 'Cache-Control' , expr );

  res.set( 'Pragma' , 'no-cache' );
  
  res.set( 'Expires', new Date(Date.now() + (1000 * secs) ).toUTCString() ); 

  next();
};