module.exports = (req, res, next) => { 

  // ----------------------------------------------------------------------
  // | Content Security Policy (CSP)                                      |
  // Mitigate the risk of cross-site scripting and other content-injection attacks.
  // ----------------------------------------------------------------------

  res.set( 'Content-Security-Policy' , "script-src 'self'; object-src 'self'" );

  // "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';"  
  // "default-src 'self'; style-src 'self' fonts.googleapis.com 'unsafe-inline' ; child-src 'self'; script-src 'self'; img-src 'self' data: ; connect-src 'self'; media-src 'self'; object-src 'self'; font-src 'self' fonts.gstatic.com "

  next();
};