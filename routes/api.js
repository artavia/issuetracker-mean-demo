// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// process.env SETUP
// =============================================
const { 
  JWT_SECRET 
} = process.env;

// =============================================
// BASE SETUP
// =============================================
const express = require("express");
const apiRouter = express.Router();
const jwt = require('jsonwebtoken');

// =============================================
// MODEL DEFINITION
// =============================================
const User = require('../custom_models_mongoose/user.model');
const Event = require('../custom_models_mongoose/event.model');
const Specialevent = require('../custom_models_mongoose/specialevent.model');

// Import UNUTILISED verifyToken helper function
// =============================================
// const verifyToken = require( '../custom_middleware/verifyToken' );

// =============================================
// ROUTING ASSIGNMENTS
// =============================================
apiRouter.get( '/users' , async ( req, res ) => {
  User.find( ( err, users ) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data."} );
    }
    res.status(200).json( users );
  } ); 
} ); 

apiRouter.get( '/events' , ( req, res ) => {

  Event.find( ( err, events ) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data."} );
    }
    res.status(200).json( events ); 
    
  } );
} );

apiRouter.get( '/special' , /* verifyToken , */ ( req, res ) => {

  Specialevent.find( ( err, specialevents ) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data."} );
    } 
    res.status(200).json( specialevents ); 

  } );
} );

// MODIFIED
apiRouter.post( '/register' , ( req, res, next ) => {  
  
  // let user = new User( req.body );
  let userData = req.body;
  let user = new User( userData );

  user.save( ( err, registeredUser ) => { 
    if(err){ // console.log( "err", err );
      res.status(500).json( { errormessage:  "Internal Error -- problem in posting data."} );
    }

    res.status(200).json( { message : "User has been added" } );

    // let payload = { custom_subject: registeredUser._id };
    // let token = jwt.sign( payload, JWT_SECRET );
    // res.status(200).send( { token } );

  } );
} );


apiRouter.post( '/login' , ( req, res, next ) => {
  
  const { email, password } = req.body;
  let paramObj = { email: email };

  User.findOne( paramObj , ( err, user ) => {
    
    if(err){ 
      // res.status(500); // NetworkError when attempting to fetch resource.
      res.status(500).json( { errormessage : "Internal Error -- problem in posting data."} );
    } 
    
    if( !user ) { 
      // console.log("if not user...");
      // res.status(401); // Unauthorized
      // res.status(401).json( { errormessage: "Invalid Email. Go register!" } ); 
      res.json( { errormessage: "Invalid Email. Go register!" } ); 
    } 

    if( user ){ 

      // EASE BACK IN...
      // let payload = { custom_subject: user._id };
      // let token = jwt.sign( payload, JWT_SECRET );
      // res.status(200).send( { token : token } );
      // end of EASE!

      // user.comparePdub( password, function(err,isMatch){
      // } );

      user.comparePdub( password, function(err,isMatch){
        if(err){  
          // console.log( "user.comparePdub... if... err" , err ); 
          res.status(500).json( { errormessage: "Internal Error -- problem in confirming data."} );
        } 
        
        if( isMatch === false ){ 
          // console.log("if isMatch === false...");
          // res.status(404); // Not Found
          // res.status(404).json( { errormessage: "Invalid Password. Try again!" } ); 
          res.json( { errormessage: "Invalid Password. Try again!" } ); 
        }
        
        if( isMatch === true ){
          
          // console.log("req.body.email" , email );
          // console.log("user" , user );
          
          jwt.sign( { user }, JWT_SECRET, { expiresIn: '1h' } , ( err, token ) => {
            if( err ) { 
              // console.log( "jwt sign err" , err ); 
              res.json( { errormessage : `${err}` } );
            }

            res.status(200).json( { token: token } ); 
            // here...

            // EASE some here...

          } );

        }
      } );

    }

  } );
} );


module.exports = apiRouter;