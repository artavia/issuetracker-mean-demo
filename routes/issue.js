// BASE SETUP
// =============================================
const express = require("express");
const issueRouter = express.Router();

// MODEL DEFINITION
// =============================================
const Issue = require("../custom_models_mongoose/issue.model");

// ROUTING ASSIGNMENTS
// =============================================
issueRouter.get( "/" , ( req, res ) => {
  Issue.find( (err, issues) => {
    if(err){// console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data." } );
    }
    res.status(200).json( issues );
  } );
} );

issueRouter.get( "/:id", ( req, res ) => {
  
  let id = req.params.id;

  Issue.findById( id, ( err, issue ) => {
    if(err){// console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data." } );
    }
    res.status(200).json(issue);
  } );
} );

issueRouter.post( "/add" , ( req, res ) => {

  /* 
    NG8 issues
  {
   "title" : "Freezerbox issues"
   , "responsible" : "Lucho"
   , "description" : "Ran out of chocolate ice cream"
   , "severity" : "High"
   , "status" : "Open"
  } 
  */

  // console.log( "req.body" , req.body ); 
  
  let issue = new Issue( req.body );
  issue.save( ( err, registeredIssue ) => {
    if(err){ // console.log( "err", err );
      res.status(500).json( { errormessage:  "Internal Error -- problem in posting data." } );
    }
    res.status(200).json( { message : "Issue has been added" } );
  } );

} );


issueRouter.post( "/update/:id" , ( req, res ) => {
  
  let id = req.params.id;

  Issue.findById( id, ( err, issue ) => {
    if(!issue){
      return res.status(404).send( { errormessage : "Issue not found" } );
    }

    issue.title = req.body.title;
    issue.responsible = req.body.responsible;
    issue.description = req.body.description;
    issue.severity = req.body.severity;
    issue.status = req.body.status;

    issue.save( ( err, updatedIssue ) => {
      if(err){ // console.log( "err", err );
        res.status(500).json( { errormessage:  "Internal Error -- problem in updating data." } );
      }
      res.status(200).json( { message : "Issue has been updated" } );
    } );

  } );
} );

issueRouter.get( "/delete/:id", ( req, res ) => {

  let idObjParameter = { _id: req.params.id };
  
  Issue.findByIdAndRemove( idObjParameter, ( err, issue ) => {
    if(err){ // console.log( "err" , err );
      res.status(500).json( { errormessage: "Internal Error -- problem loading data." } );
    }
    res.status(200).json( { message : "Issue removed successfully" } );
  } );

} );

module.exports = issueRouter;