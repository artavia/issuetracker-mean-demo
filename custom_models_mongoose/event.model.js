const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let EventSchema = new Schema( {
  name: { type: String }
  , description: { type: String }
  , date: { type: Date }
} );

module.exports = mongoose.model( "Event" , EventSchema );