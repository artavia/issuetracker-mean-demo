const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let Issue = new Schema( {
  
  title: { type: String }
  , responsible: { type: String }
  , description: { type: String } 
  , severity: { type: String } 
  , status: { type: String, default: 'Open' }

  // , completed: { type: Boolean , default: false } 
} );

module.exports = mongoose.model( "Issue" , Issue );